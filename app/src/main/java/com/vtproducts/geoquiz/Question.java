package com.vtproducts.geoquiz;

public class Question {

    private int mTextResId;
    private boolean mAnswerTrue;
    private boolean mHasInput;
    private boolean mWasCheated;

    public int getTextResId() {
        return mTextResId;
    }

    public void setTextResId(int textResId) {
        mTextResId = textResId;
    }

    public boolean isAnswerTrue() {
        return mAnswerTrue;
    }

    public void setAnswerTrue(boolean answerTrue) {
        mAnswerTrue = answerTrue;
    }

    public boolean getHasInput() {
        return mHasInput;
    }

    public void setHasInput(boolean hasInput) {
        mHasInput = hasInput;
    }

    public void setWasCheated(boolean wasCheated) {
        mWasCheated = wasCheated;
    }

    public boolean getWasCheated() {
        return mWasCheated;
    }

    public Question (int textResId, boolean answerTrue, boolean hasInput, boolean wasCheated) {
        mTextResId = textResId;
        mAnswerTrue = answerTrue;
        mHasInput = hasInput;
        mWasCheated = wasCheated;
    }
}
