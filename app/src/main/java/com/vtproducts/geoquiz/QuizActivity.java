package com.vtproducts.geoquiz;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.text.AttributedCharacterIterator;

public class QuizActivity extends AppCompatActivity {

    private static final String TAG = "QuizActivity";
    private static final String KEY_INDEX = "index";
    private static final String KEY_CORRECT_ANSWERS_INDEX = "CorrectAnswersIndex";
    private static final String KEY_QUESTIONS_ANSWERED_INDEX = "QuestionsAnsweredIndex";
    private static final String KEY_HAS_INPUT_INDEX = "HasInputIndex";
    private static final String KEY_IS_CHEATER_INDEX = "IsCheaterIndex";
    private static final int REQUEST_CODE_CHEAT = 0;
    private static final int MAX_CHEAT_TOKENS = 3;
    private Button mTrueButton;
    private Button mFalseButton;
    private Button mNextButton;
    private Button mPreviousButton;
    private Button mCheatButton;
    private TextView mQuestionTextView;
    private TextView mCheatTokens;


    private Question[] mQuestionBank = new Question[]{
            new Question(R.string.question_australia, true, false, false),
            new Question(R.string.question_oceans, true, false, false),
            new Question(R.string.question_mideast, false, false, false),
            new Question(R.string.question_africa, false, false, false),
            new Question(R.string.question_americas, true, false, false),
            new Question(R.string.question_asia, true, false, false),
    };

    private int mCurrentIndex = 0;
    private int mCorrectAnswers = 0;
    private int mQuestionsAnswered = 0;
    private int mCheatTokensUsed = 0;
    private boolean mIsCheater;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate(Bundle) called");
        setContentView(R.layout.activity_quiz);

        if (savedInstanceState != null) {
            mCurrentIndex = savedInstanceState.getInt(KEY_INDEX, 0);
            mCorrectAnswers = savedInstanceState.getInt(KEY_CORRECT_ANSWERS_INDEX, 0);
            mQuestionsAnswered = savedInstanceState.getInt(KEY_QUESTIONS_ANSWERED_INDEX, 0);

            boolean[] hasAnswers = savedInstanceState.getBooleanArray(KEY_HAS_INPUT_INDEX);
            boolean[] wasCheated = savedInstanceState.getBooleanArray(KEY_IS_CHEATER_INDEX);
            if (hasAnswers != null) {
                for (int i = 0; i < mQuestionBank.length; i++) {
                    mQuestionBank[i].setHasInput(hasAnswers[i]);
                    mQuestionBank[i].setWasCheated(wasCheated[i]);
                    if (wasCheated[i]) {
                        mCheatTokensUsed = mCheatTokensUsed + 1;
                    }
                }
            }
        }

        mQuestionTextView = (TextView) findViewById(R.id.question_text_view);
        mQuestionTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                incrementCurrentIndex();
                updateQuestion();
            }
        });

        mTrueButton = (Button) findViewById(R.id.true_button);
        if (mQuestionBank[mCurrentIndex].getHasInput() == true) {
            mTrueButton.setEnabled(false);
        }
        mTrueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(true);
            }
        });

        mFalseButton = (Button) findViewById(R.id.false_button);
        mFalseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(false);
            }
        });

        mCheatButton = (Button) findViewById(R.id.cheat_button);
        mCheatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean answerIsTrue = mQuestionBank[mCurrentIndex].isAnswerTrue();
                Intent intent = CheatActivity.newIntent(QuizActivity.this, answerIsTrue);
                startActivityForResult(intent, REQUEST_CODE_CHEAT);
            }
        });

        mCheatTokens = (TextView) findViewById(R.id.cheat_tokens);
        //mCheatTokens.setText(getString(R.string.cheat_tokens) + ": " + (MAX_CHEAT_TOKENS - mCheatTokensUsed));

        mNextButton = (Button) findViewById(R.id.next_button);
        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                incrementCurrentIndex();
                updateQuestion();
                Log.d(TAG, "Cheats rem: " + (MAX_CHEAT_TOKENS - mCheatTokensUsed));
                enableOrDisableButtons();
            }
        });

        mPreviousButton = (Button) findViewById(R.id.previous_button);
        mPreviousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                decrementCurrentIndex();
                updateQuestion();
                enableOrDisableButtons();
            }
        });
        enableOrDisableButtons();
        updateQuestion();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        if (requestCode == REQUEST_CODE_CHEAT) {
            if (data == null) {
                return;
            }
            mIsCheater = CheatActivity.wasAnswerShown(data);
            mQuestionBank[mCurrentIndex].setWasCheated(mIsCheater);
            if (mIsCheater) {
                mCheatTokensUsed = mCheatTokensUsed + 1;
                updateQuestion();
            }

            enableOrDisableButtons();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart() called");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume() called");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause() called");
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        Log.i(TAG, "onSaveInstanceState");
        savedInstanceState.putInt(KEY_INDEX, mCurrentIndex);
        savedInstanceState.putInt(KEY_CORRECT_ANSWERS_INDEX, mCorrectAnswers);
        savedInstanceState.putInt(KEY_QUESTIONS_ANSWERED_INDEX, mQuestionsAnswered);
        //savedInstanceState.putBoolean(KEY_IS_CHEATER_INDEX, mIsCheater);

        boolean[] hasInput = new boolean[mQuestionBank.length];
        boolean[] isCheater = new boolean[mQuestionBank.length];
        for (int i = 0; i < mQuestionBank.length; i++) {
            hasInput[i] = mQuestionBank[i].getHasInput();
            isCheater[i] = mQuestionBank[i].getWasCheated();
        }
        savedInstanceState.putBooleanArray(KEY_HAS_INPUT_INDEX, hasInput);
        savedInstanceState.putBooleanArray(KEY_IS_CHEATER_INDEX, isCheater);
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop() called");
        
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy() called");
    }

    private void updateQuestion() {
        int question = mQuestionBank[mCurrentIndex].getTextResId();
        mQuestionTextView.setText(question);
        mCheatTokens.setText(getString(R.string.cheat_tokens) + ": " + (MAX_CHEAT_TOKENS - mCheatTokensUsed));
    }

    private void incrementCurrentIndex() {
        mCurrentIndex = (mCurrentIndex + 1) % mQuestionBank.length;
    }

    private void decrementCurrentIndex() {
        if (mCurrentIndex == 0) {
            mCurrentIndex = mQuestionBank.length - 1;
        } else {
            mCurrentIndex = (mCurrentIndex - 1) % mQuestionBank.length;
        }
    }

    private void enableOrDisableButtons () {
        if (mQuestionBank[mCurrentIndex].getHasInput() == true) {
            mTrueButton.setEnabled(false);
            mFalseButton.setEnabled(false);
        } else {
            mTrueButton.setEnabled(true);
            mFalseButton.setEnabled(true);
        }

        if (mCheatTokensUsed >= 3) {
            mCheatButton.setEnabled(false);
        } else {
            mCheatButton.setEnabled(true);
        }

    }

    private void calculateScore() {
        double percentage = ((double)mCorrectAnswers/(double)mQuestionsAnswered) * 100;
        String toastMessage = getString(R.string.score_statement_part1) + String.format("%.0f", percentage) + "%";

        Toast score = Toast.makeText(this,
                toastMessage,
                Toast.LENGTH_LONG);
        score.setGravity(Gravity.TOP, 0, 0);
        score.show();

        restart();
    }

    private void restart() {
        mCurrentIndex = 0;
        mQuestionsAnswered = 0;
        mCorrectAnswers = 0;
        mCheatTokensUsed = 0;
        for (int i = 0; i < mQuestionBank.length; i++ ) {
            mQuestionBank[i].setHasInput(false);
            mQuestionBank[i].setWasCheated(false);
        }
        updateQuestion();
        enableOrDisableButtons();
    }

    private void checkAnswer (boolean userPressedTrue) {
        boolean answerIsTrue = mQuestionBank[mCurrentIndex].isAnswerTrue();

        mQuestionBank[mCurrentIndex].setHasInput(true);
        enableOrDisableButtons();

        int messageResId = 0;

        if (mQuestionBank[mCurrentIndex].getWasCheated()) {
            messageResId = R.string.judgment_toast;
        } else {
            if (userPressedTrue == answerIsTrue) {
                messageResId = R.string.correct_toast;
                mCorrectAnswers = mCorrectAnswers + 1;

            } else {
                messageResId = R.string.incorrect_toast;
            }
        }



        mQuestionsAnswered = mQuestionsAnswered + 1;

        Toast message = Toast.makeText(this, messageResId,
                Toast.LENGTH_SHORT);
        message.setGravity(Gravity.BOTTOM, 0, 0);
        message.show();

        if (mQuestionsAnswered == mQuestionBank.length) {
            calculateScore();
        }
    }
}